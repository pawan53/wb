import { WorldBankPage } from './app.po';

describe('world-bank App', () => {
  let page: WorldBankPage;

  beforeEach(() => {
    page = new WorldBankPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
