var express = require('express');  
var app = express();  
  
app.use(express.static("dist"));  
  
app.get('/', function (req, res) {  
    res.redirect('/');  
});  
  
app.listen(8080, '0.0.0.0');  
console.log("MyProject Server is Listening on port 8080");  