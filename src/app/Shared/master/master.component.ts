﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginComponent } from '../../login/login.component';
import { AuthGuard } from '../../Services/auth.guard';
import { Router } from '@angular/router';
@Component({
    selector: 'master',
    templateUrl: './master.component.html',
    styleUrls: ['./master.component.css']
})
export class MasterComponent {

    public isAuth: boolean = false;
    //constructor( private router: Router) { }
    constructor(private authGuard: AuthGuard, private router: Router) { }

    ngOnInit() {
        this.isAuth = this.authGuard.isUserAuthenticated();
    }

    public setIsAuth(isauth: boolean) {
        this.isAuth = isauth;
    }

    LogOut() {
        this.isAuth = false;
        localStorage.removeItem("isAuth");
        this.router.navigate(['/login']);
    }

    routeToDashboard() {
        this.router.navigate(['/dashboard']);
    }

    get currentPage() {
        let currentPageName = localStorage.getItem("currentRoute");
        switch (currentPageName) {
            case 'dashboard': currentPageName = 'Enterprise Search'; break;
            case 'search': currentPageName = 'Search'; break;
            case 'discover': currentPageName = 'Discover'; break;
            case 'analyze': currentPageName = 'Analyze'; break;
            default: currentPageName = currentPageName; break;
        }
        return currentPageName;
    }
    
}
