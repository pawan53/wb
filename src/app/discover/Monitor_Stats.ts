export const monitor_stats = [
  {
    "id": "call",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 10 },
      { "date": new Date("2017-09-16"), "statistics": 13 },
      { "date": new Date("2017-09-17"), "statistics": 11 },
      { "date": new Date("2017-09-18"), "statistics": 12 },
      { "date": new Date("2017-09-19"), "statistics": 9 },
      { "date": new Date("2017-09-20"), "statistics": 13 },
      { "date": new Date("2017-09-21"), "statistics": 10 },
      { "date": new Date("2017-09-22"), "statistics": 11 },
      { "date": new Date("2017-09-23"), "statistics": 12 },
      { "date": new Date("2017-09-24"), "statistics": 9 }
    ]
  },
  {
    "id": "development",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 54 },
      { "date": new Date("2017-09-16"), "statistics": 55 },
      { "date": new Date("2017-09-17"), "statistics": 53 },
      { "date": new Date("2017-09-18"), "statistics": 57 },
      { "date": new Date("2017-09-19"), "statistics": 55 },
      { "date": new Date("2017-09-20"), "statistics": 112 },
      { "date": new Date("2017-09-21"), "statistics": 60 },
      { "date": new Date("2017-09-22"), "statistics": 54 },
      { "date": new Date("2017-09-23"), "statistics": 57 },
      { "date": new Date("2017-09-24"), "statistics": 56 }
    ]
  },
  {
    "id": "banking",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 25 },
      { "date": new Date("2017-09-16"), "statistics": 24 },
      { "date": new Date("2017-09-17"), "statistics": 25 },
      { "date": new Date("2017-09-18"), "statistics": 27 },
      { "date": new Date("2017-09-19"), "statistics": 26 },
      { "date": new Date("2017-09-20"), "statistics": 23 },
      { "date": new Date("2017-09-21"), "statistics": 25 },
      { "date": new Date("2017-09-22"), "statistics": 27 },
      { "date": new Date("2017-09-23"), "statistics": 23 },
      { "date": new Date("2017-09-24"), "statistics": 25 }
    ]
  },
  {
    "id": "loan",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 120 },
      { "date": new Date("2017-09-16"), "statistics": 130 },
      { "date": new Date("2017-09-17"), "statistics": 100 },
      { "date": new Date("2017-09-18"), "statistics": 124 },
      { "date": new Date("2017-09-19"), "statistics": 129 },
      { "date": new Date("2017-09-20"), "statistics": 121 },
      { "date": new Date("2017-09-21"), "statistics": 130 },
      { "date": new Date("2017-09-22"), "statistics": 110 },
      { "date": new Date("2017-09-23"), "statistics": 125 },
      { "date": new Date("2017-09-24"), "statistics": 123 }
    ]
  },
  {
    "id": "health",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 9 },
      { "date": new Date("2017-09-16"), "statistics": 8 },
      { "date": new Date("2017-09-17"), "statistics": 6 },
      { "date": new Date("2017-09-18"), "statistics": 11 },
      { "date": new Date("2017-09-19"), "statistics": 9 },
      { "date": new Date("2017-09-20"), "statistics": 10 },
      { "date": new Date("2017-09-21"), "statistics": 11 },
      { "date": new Date("2017-09-22"), "statistics": 8 },
      { "date": new Date("2017-09-23"), "statistics": 9 },
      { "date": new Date("2017-09-24"), "statistics": 8 }
    ]
  },
  {
    "id": "reforms",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 35 },
      { "date": new Date("2017-09-16"), "statistics": 33 },
      { "date": new Date("2017-09-17"), "statistics": 30 },
      { "date": new Date("2017-09-18"), "statistics": 34 },
      { "date": new Date("2017-09-19"), "statistics": 37 },
      { "date": new Date("2017-09-20"), "statistics": 34 },
      { "date": new Date("2017-09-21"), "statistics": 32 },
      { "date": new Date("2017-09-22"), "statistics": 31 },
      { "date": new Date("2017-09-23"), "statistics": 38 },
      { "date": new Date("2017-09-24"), "statistics": 32 }
    ]
  },
  {
    "id": "program",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 93 },
      { "date": new Date("2017-09-16"), "statistics": 92 },
      { "date": new Date("2017-09-17"), "statistics": 98 },
      { "date": new Date("2017-09-18"), "statistics": 95 },
      { "date": new Date("2017-09-19"), "statistics": 91 },
      { "date": new Date("2017-09-20"), "statistics": 200 },
      { "date": new Date("2017-09-21"), "statistics": 96 },
      { "date": new Date("2017-09-22"), "statistics": 94 },
      { "date": new Date("2017-09-23"), "statistics": 91 },
      { "date": new Date("2017-09-24"), "statistics": 95 }
    ]
  },
  {
    "id": "finance",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 20 },
      { "date": new Date("2017-09-16"), "statistics": 19 },
      { "date": new Date("2017-09-17"), "statistics": 22 },
      { "date": new Date("2017-09-18"), "statistics": 18 },
      { "date": new Date("2017-09-19"), "statistics": 21 },
      { "date": new Date("2017-09-20"), "statistics": 20 },
      { "date": new Date("2017-09-21"), "statistics": 19 },
      { "date": new Date("2017-09-22"), "statistics": 22 },
      { "date": new Date("2017-09-23"), "statistics": 24 },
      { "date": new Date("2017-09-24"), "statistics": 18 }
    ]
  },
  {
    "id": "policy",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 73 },
      { "date": new Date("2017-09-16"), "statistics": 71 },
      { "date": new Date("2017-09-17"), "statistics": 74 },
      { "date": new Date("2017-09-18"), "statistics": 69 },
      { "date": new Date("2017-09-19"), "statistics": 70 },
      { "date": new Date("2017-09-20"), "statistics": 73 },
      { "date": new Date("2017-09-21"), "statistics": 72 },
      { "date": new Date("2017-09-22"), "statistics": 69 },
      { "date": new Date("2017-09-23"), "statistics": 72 },
      { "date": new Date("2017-09-24"), "statistics": 70 }
    ]
  },
  {
    "id": "government",
    "values": [
      { "date": new Date("2017-09-15"), "statistics": 8 },
      { "date": new Date("2017-09-16"), "statistics": 7 },
      { "date": new Date("2017-09-17"), "statistics": 5 },
      { "date": new Date("2017-09-18"), "statistics": 6 },
      { "date": new Date("2017-09-19"), "statistics": 10 },
      { "date": new Date("2017-09-20"), "statistics": 11 },
      { "date": new Date("2017-09-21"), "statistics": 7 },
      { "date": new Date("2017-09-22"), "statistics": 12 },
      { "date": new Date("2017-09-23"), "statistics": 7 },
      { "date": new Date("2017-09-24"), "statistics": 13 }
    ]
  }
];

export const monitor_stats2 = [
  { "id": "call", "values": [{ "date": new Date("2017-09-15"), "statistics": 10 }, { "date": new Date("2017-09-16"), "statistics": 13 }] },
  { "id": "development", "values": [{ "date": new Date("2017-09-15"), "statistics": 54 }, { "date": new Date("2017-09-16"), "statistics": 55 }] },
  { "id": "banking", "values": [{ "date": new Date("2017-09-15"), "statistics": 25 }, { "date": new Date("2017-09-16"), "statistics": 24 }] },
  { "id": "loan", "values": [{ "date": new Date("2017-09-15"), "statistics": 120 }, { "date": new Date("2017-09-16"), "statistics": 130 }] },
  { "id": "health", "values": [{ "date": new Date("2017-09-15"), "statistics": 9 }, { "date": new Date("2017-09-16"), "statistics": 8 }] },
  { "id": "reforms", "values": [{ "date": new Date("2017-09-15"), "statistics": 35 }, { "date": new Date("2017-09-16"), "statistics": 33 }] },
  { "id": "program", "values": [{ "date": new Date("2017-09-15"), "statistics": 93 }, { "date": new Date("2017-09-16"), "statistics": 92 }] },
  { "id": "finance", "values": [{ "date": new Date("2017-09-15"), "statistics": 20 }, { "date": new Date("2017-09-16"), "statistics": 19 }] },
  { "id": "policy", "values": [{ "date": new Date("2017-09-15"), "statistics": 73 }, { "date": new Date("2017-09-16"), "statistics": 71 }] },
  { "id": "government", "values": [{ "date": new Date("2017-09-15"), "statistics": 8 }, { "date": new Date("2017-09-16"), "statistics": 7 }] }
];