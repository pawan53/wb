import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import * as d3 from 'd3';
import * as d3Selection from 'd3-selection';
import * as d3Scale from "d3-scale";
import * as d3Shape from "d3-shape";
import * as d3Array from "d3-array";
import * as d3Axis from "d3-axis";
import { monitor_stats, monitor_stats2 } from './monitor_stats';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.css']
})
export class DiscoverComponent implements OnInit {

  constructor() { }

  private discoverTabArray: any = [
    { text: "Trending", value: 0 },
    { text: "Monitoring", value: 1 },
    { text: "News Trends", value: 2 },
  ];

  selectedIndex: number;

  svg: any; g: any; width: number; height: number; x; y; z; line;
  data: any; margin = { top: 20, right: 80, bottom: 30, left: 50 };
  timer: any; subscription: any; index: number = 2;

  ngOnInit() {
    let tabToLoad = 0;
    this.onChangeTab(tabToLoad);
  }

  toCamelCase(str: any) {
    return str.toLowerCase().replace(/(?:(^.)|(\s+.))/g, function (match) {
      return match.charAt(match.length - 1).toUpperCase();
    });
  }

  public onChangeTab(index: number) {
    this.selectedIndex = index;
    if (this.subscription) this.subscription.unsubscribe();
    d3.select(".ledg-row").remove(".ledg-row");
    switch (index) {
      case 0: break;
      case 1: this.getSpikeChart(); break;
      case 2: this.drawTreeChart(); break;
      default: break;
    }
  }

  public getSpikeChart() {
    setTimeout(() => {
      this.index = 2;
      this.data = monitor_stats.map((v) => v.values.map((v) => v.date))[0];
      this.initSpikeChart();
      this.svg = d3Selection.select("svg");
      monitor_stats2.forEach(function (val, ind) { monitor_stats2[ind].values = monitor_stats2[ind].values.slice(0, 2); })

      this.timer = Observable.interval(5000);
      this.subscription = this.timer.subscribe(() => {
        if (this.index == 11) {
          this.svg.selectAll(".dataType").remove();
          monitor_stats2.forEach(function (val, ind) { monitor_stats2[ind].values = monitor_stats2[ind].values.slice(0, 2); })
          this.index = 2;
        } else {
          this.svg.selectAll("*").remove();
          this.initSpikeChart();
          this.drawPath();
          let index = this.index;
          monitor_stats.forEach(function (val, ind) { monitor_stats2[ind].values.push(val.values[index]); })
          this.index++;
        }
      });
    }, 100);
  }

  private initSpikeChart(): void {
    this.svg = d3Selection.select("svg");
    this.svg.selectAll("*").remove();

    this.width = this.svg.attr("width") - this.margin.left - this.margin.right;
    this.height = this.svg.attr("height") - this.margin.top - this.margin.bottom;

    this.g = this.svg.append("g").attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    this.x = d3Scale.scaleTime().range([0, this.width - 100]);
    this.y = d3Scale.scaleLinear().range([this.height, 0]);
    this.z = d3Scale.scaleOrdinal(d3Scale.schemeCategory10);

    this.line = d3Shape.line()
      .x((d: any) => this.x(d.date))
      .y((d: any) => this.y(d.statistics));

    this.x.domain(d3Array.extent(this.data, (d: Date) => d));

    this.y.domain([
      d3Array.min(monitor_stats, function (c) { return d3Array.min(c.values, function (d) { return d.statistics; }); }),
      d3Array.max(monitor_stats, function (c) { return d3Array.max(c.values, function (d) { return d.statistics; }); })
    ]);

    this.z.domain(monitor_stats.map(function (c) { return c.id; }));

    this.g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3Axis.axisBottom(this.x));

    this.g.append("g")
      .attr("class", "axis axis--y")
      .call(d3Axis.axisLeft(this.y))
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("fill", "#000");
  }

  drawPath() {
    let legendItemSize = 15;
    let legendSpacing = 5;
    let dataType = this.g.selectAll(".dataType")
      .data(monitor_stats2)
      .enter().append("g")
      .attr("class", "dataType");

    dataType.append("path")
      .attr("class", "line")
      .attr("d", (d) => this.line(d.values))
      .style("stroke", (d) => this.z(d.id));

    let legend = this.svg
      .selectAll('.legend')
      .data(monitor_stats2)
      .enter()
      .append('g')
      .attr('class', 'legend')
      .attr('transform', (d, i) => {
        let height = legendItemSize + legendSpacing
        let offset = height * this.z(d.id).length / 10
        let x = legendItemSize * 55;
        let y = ((i + 10) * height) - offset
        return `translate(${x}, ${y})`
      });

    legend
      .append('rect')
      .attr('width', legendItemSize)
      .attr('height', legendItemSize)
      .style('fill', (d) => this.z(d.id));

    legend
      .append('text')
      .attr('x', legendItemSize + legendSpacing)
      .attr('y', legendItemSize - legendSpacing)
      .text((d) => d.id)
  }

  private drawTreeChart(): void {
    let that = this;
    setTimeout(() => {
      var svg = d3.select("svg"), width = +svg.attr("width"), height = +svg.attr("height");
      svg.selectAll("*").remove();

      var fader = function (color) { return d3.interpolateRgb(color, "#fff")(0.2); },
        color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
        format = d3.format(",d");

      var treemap = d3.treemap()
        .tile(d3.treemapResquarify)
        .size([width, height])
        .round(true)
        .paddingInner(1);



      d3.json("assets/top_words.json", function (error, data) {
        if (error) throw error;

        var root = d3.hierarchy(data)
          .eachBefore(function (d) { d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name; })
          .sum(function (d) { return d.size; })
          .sort(function (a, b) { return b.height - a.height || b.value - a.value; });

        treemap(root);

        var cell = svg.selectAll("g")
          .data(root.leaves())
          .enter().append("g")
          .attr("transform", function (d) { return "translate(" + d.x0 + "," + d.y0 + ")"; });

        cell.append("rect")
          .attr("id", function (d) { return d.data.id; })
          .attr("width", function (d) { return d.x1 - d.x0; })
          .attr("height", function (d) { return d.y1 - d.y0; })
          .attr("fill", function (d) { return color(d.parent.data.id); });

        cell.append("clipPath")
          .attr("id", function (d) { return "clip-" + d.data.id; })
          .append("use")
          .attr("xlink:href", function (d) { return "#" + d.data.id; });

        cell.append("text")
          .attr("clip-path", function (d) { return "url(#clip-" + d.data.id + ")"; })
          .selectAll("tspan")
          .data(function (d) { return d.data.name.split(/(?=[A-Z][^A-Z])/g); })
          .enter().append("tspan")
          .attr("x", 4)
          .attr("y", function (d, i) { return 13 + i * 10; })
          .text(function (d) { return d; });

        cell.append("title").text(function (d) { return that.toCamelCase(d.parent.data.name); });

        var ledgColors = ["#4C92C3", "#FF993E", "#FFC993", "#56B356", "#BED2ED", "#ADE5A1", "#DE5253", "#FFADAB", "#A985CA", "#D1C0DD"]
          , ledgLabels = ["china", "iraq", "ukraine", "guinea", "malaysia", "india", "japan", "argentina", "australia", "canada"]

        var ledg = d3.select(".discover-chart").append("div")
          .style("position", "relative")
          .attr("class", "ledg-row");

        for (let i = 0; i < 10; i++) {
          ledg.append('div')
            .attr("class", "ledg")
            .style("width", "80px")
            .style("height", "20px")
            .style("top", function (d) { return (25 * i) + "px" })
            .text(function (d) { return ledgLabels[i] })
            .style("background", function (d) { return ledgColors[i] })
        };
      });
    }, 10);
  }
}
