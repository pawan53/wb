﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, Route, ActivatedRouteSnapshot } from '@angular/router'

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }
    canActivate(route: ActivatedRouteSnapshot) {

        if (route.url.length > 0) {
            let currentRoute = route.url[0].path;
            localStorage.setItem("currentRoute", currentRoute);
        }
        
        if (localStorage.getItem("isAuth") == null || localStorage.getItem("isAuth") == "undefined" || localStorage.getItem("isAuth") == "") {
            this.router.navigate(['/login']);
            return false;
        }
        if (localStorage.getItem("isAuth") == "true") {
            return true;
        }

        return false;
    }

    isUserAuthenticated(): boolean {
        if (localStorage.getItem("isAuth") == null || localStorage.getItem("isAuth") == "undefined" || localStorage.getItem("isAuth") == "") {
            return false;
        }
        if (localStorage.getItem("isAuth") == "true") {
            return true;
        }
        return false;
    }


}
