﻿import { Injectable } from '@angular/core';
import { Observable, } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { HttpService } from './http.service';
import { Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Router } from "@angular/router";
import { AuthGuard } from "./auth.guard";


@Injectable()
export class ApiService {

    public dev = 'http://127.0.0.1:9090';
    public prod = 'http://ec2-13-229-107-139.ap-southeast-1.compute.amazonaws.com:8080';//TODO

    public baseURL = this.dev;
    public appURL = "worldBank";

    constructor(private httpService: HttpService, private router: Router) {
    }

    public download(fileName) {
        var form = document.createElement("form");
        var elementButton = document.createElement("button"); 

        let url = this.baseURL + "/" + this.appURL + "/pdf/external/" + fileName;

        form.method = "GET";
        form.action = url;   
        form.target = "_blank";

        elementButton.value="button";
        elementButton.name="type";
        form.appendChild(elementButton);  

        document.body.appendChild(form);

        form.submit();

        form.parentNode.removeChild(form);
    }

    public login(username, password): Promise<any> {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");

        let body = { username: username, password: password };
        let url = this.baseURL + "/" + this.appURL + "/login";
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
            this.httpService.post(url, JSON.stringify(body), options)
                .map(res => res.json())
                .subscribe(
                data => {
                    resolve(data);
                },
                err => {
                    reject(err)
                }
                )
        })
    }

    public search(documentTypes, sources, includeTags, excludeTags, pageNumber): Promise<any> {

        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let body = { documentTypes: documentTypes, sources: sources, includeKeyWords: includeTags, excludeKeywords: excludeTags, page: pageNumber };

        let url = this.baseURL + "/" + this.appURL + "/search";
        let options = new RequestOptions({ headers: headers, withCredentials: true });

        return new Promise((resolve, reject) => {
            this.httpService.post(url, JSON.stringify(body), options)
                .map(res => res.json())
                .subscribe(
                data => {
                    resolve(data);
                },
                err => {
                    reject(err)
                }
                )
        })
    }

    public summarizeText(text, sentences): Promise<any> {

        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let body = { inputText: text, sentences: sentences };

        let url = this.baseURL + "/" + this.appURL + "/summarize/text";
        let options = new RequestOptions({ headers: headers, withCredentials: true });

        return new Promise((resolve, reject) => {
            this.httpService.post(url, JSON.stringify(body), options)
                .map(res => res)
                .subscribe(
                data => {
                    resolve(data);
                },
                err => {
                    reject(err)
                }
                )
        })
    }

    public summarizeWebUrl(weburl, sentences): Promise<any> {

        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let body = { weburl: weburl, sentences: sentences };

        let url = this.baseURL + "/" + this.appURL + "/summarize/web";
        let options = new RequestOptions({ headers: headers, withCredentials: true });

        return new Promise((resolve, reject) => {
            this.httpService.post(url, JSON.stringify(body), options)
                .map(res => res)
                .subscribe(
                data => {
                    resolve(data);
                },
                err => {
                    reject(err)
                }
                )
        })
    }

    public sentimentText(text): Promise<any> {

        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let body = { text: text };

        let url = "http://ec2-13-229-107-139.ap-southeast-1.compute.amazonaws.com:5000/analyze/sentiment/text";
        let options = new RequestOptions({ headers: headers, withCredentials: true });

        return new Promise((resolve, reject) => {
            this.httpService.post(url, JSON.stringify(body), options)
                .map(res => res)
                .subscribe(
                data => {
                    resolve(data);
                },
                err => {
                    reject(err)
                }
                )
        })
    }

    public extractMeaning(text): Promise<any> {

        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let body = { text: text };

        let url = "http://ec2-13-229-107-139.ap-southeast-1.compute.amazonaws.com:5000/analyze/extractmeaning";
        let options = new RequestOptions({ headers: headers, withCredentials: true });

        return new Promise((resolve, reject) => {
            this.httpService.post(url, JSON.stringify(body), options)
                .map(res => res)
                .subscribe(
                data => {
                    resolve(data);
                },
                err => {
                    reject(err)
                }
                )
        })
    }
}
