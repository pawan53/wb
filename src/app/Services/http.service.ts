﻿import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class HttpService {

    constructor(private http: Http) {
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        var resp = this.http.get(url, { withCredentials: true });
        return resp.do(
            () => {
                console.log('httpservice: on done called');
            },
            () => {
                console.log('httpservice: on error called');
            },
            () => {
                console.log('in callback of httpservice');
            });
    }
    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        console.log('httpservice: calling server method');
        var resp: Observable<Response> = this.http.post(url, body, options);
        return resp.do(
            () => {
                console.log('httpservice: on done called');
            },
            () => {
                console.log('httpservice: on error called');
            },
            () => {
                console.log('in callback of httpservice');
            });
    }

}
