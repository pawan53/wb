﻿import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: '<master></master>',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
