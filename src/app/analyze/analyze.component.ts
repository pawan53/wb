import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../Services/dataService';
import { ApiService } from '../Services/api.service';
import * as d3 from 'd3-selection';
import * as d3Scale from "d3-scale";
import * as d3Shape from "d3-shape";

@Component({
    selector: 'app-analyze',
    templateUrl: './analyze.component.html',
    styleUrls: ['./analyze.component.css']
})
export class AnalyzeComponent implements OnInit {

    constructor(public dataservice: DataService, private apiService: ApiService, ) { }

    @ViewChild('summerizefile')
    mySummarizeInputVariable: any;

    @ViewChild('sentimentAnalysisFile')
    mySentimentAnalysisInputVariable: any;

    @ViewChild('extractMeaningFile')
    myExtractMeaningInputVariable: any;

    private margin = { top: 20, right: 20, bottom: 30, left: 50 };
    private width: number;
    private height: number;
    private radius: number;

    private arc: any;
    private labelArc: any;
    private pie: any;
    private color: any;
    private svg: any;

    analyzeTabArray: any = [
        { text: "Extract Meaning", value: 0 },
        { text: "Compute Relatedness", value: 1 },
        { text: "Summarize", value: 2 },
        { text: "Sentiment Analysis", value: 3 }];

    selectedIndex: number;
    uploadedFileContent: string;

    extractMeaningObj: ExtractMeaning = {
        enteredText: null, jsonData: null, formatData: null, resultVisible: false, textAreaDisabled: false, fileDisabled: false
    };
    computeRelatednessObj: ComputeRelatedness = { text: null, query: null, jsonData: null, formatData: null, resultVisible: false };
    summarizeObj: Summarize = {
        inputPageURL: null, pastedText: null, summarizedSentence: null, resultVisible: false, result: null, ssNumberError: false,
        ssNumberErrorMessage: null, urlDisabled: false, textAreaDisabled: false, fileDisabled: false
    };
    sentimentAnalysisObj: SentimentAnalysis = {
        enteredText: null, jsonData: null, formatData: null, resultVisible: false, textAreaDisabled: false, fileDisabled: false
    };

    ngOnInit() {
        let tabToLoad = 0;
        if (this.dataservice.searchData) {
            this.sentimentAnalysisObj.enteredText = this.dataservice.searchData.text;
            tabToLoad = 3;
        }
        this.onChangeTab(tabToLoad);
    }

    public onChangeTab(index: number) {
        this.selectedIndex = index;
    }

    public focusSummarize(focusedInput: number, inputValue: string) {
        if (inputValue && inputValue.trim().length > 1) {
            switch (focusedInput) {
                case 1:
                    this.summarizeObj.textAreaDisabled = true;
                    this.summarizeObj.fileDisabled = true;
                    break;
                case 2:
                    this.summarizeObj.urlDisabled = true;
                    this.summarizeObj.fileDisabled = true;
                    break;
                case 3:
                    this.sentimentAnalysisObj.fileDisabled = true;
                    break;
                case 4:
                    this.extractMeaningObj.fileDisabled = true;
                    break;
                default: break;
            }
        } else {
            switch (focusedInput) {
                case 1:
                    this.summarizeObj.textAreaDisabled = false;
                    this.summarizeObj.fileDisabled = false;
                    break;
                case 2:
                    this.summarizeObj.urlDisabled = false;
                    this.summarizeObj.fileDisabled = false;
                    break;
                case 3:
                    this.sentimentAnalysisObj.fileDisabled = false;
                    break;
                case 4:
                    this.extractMeaningObj.fileDisabled = false;
                    break;
                default: break;
            }
        }
    }

    public extractMeaningResult(extractMeaningObj: ExtractMeaning) {
        this.extractMeaningObj.resultVisible = false;

        let extractMeaningData: string = null;
        let extractMeaningMode: number = 0;
        if (extractMeaningObj.enteredText && extractMeaningObj.enteredText != '') {
            extractMeaningData = extractMeaningObj.enteredText;
            extractMeaningMode = 1;
        }
        else if (this.uploadedFileContent != '') {
            extractMeaningData = this.uploadedFileContent;
            extractMeaningMode = 2;
        }

        if (extractMeaningMode != 0) {
            this.apiService.extractMeaning(extractMeaningData).then(res => {
                if (res == "Error!") { console.log("Error occured at server."); return; }

                extractMeaningObj.resultVisible = true;
                let extractMeaningResultObjArray: ExtractMeaningResultModel[] = <ExtractMeaningResultModel[]>JSON.parse(JSON.parse(res._body));
                let emObj: ExtractMeaningResultModel = extractMeaningResultObjArray[0];
                this.extractMeaningObj.jsonData = '<div>' +
                    '<label>"cluster_no_1" : </label> <span>' + ' ' + emObj.cluster_no_1 + '</span><br/>' +
                    '<label>"cluster_no_1" : </label> <span>' + ' ' + emObj.cluster_no_2 + '</span><br/>' +
                    '<label>"cluster_no_3" : </label> <span>' + ' ' + emObj.cluster_no_3 + '</span><br/>' +
                    '<label>"max_prob_1" : </label> <span>' + ' ' + emObj.max_prob_1 + '</span><br/>' +
                    '<label>"max_prob_2" : </label> <span>' + ' ' + emObj.max_prob_2 + '</span><br/>' +
                    '<label>"max_prob_3" : </label> <span>' + ' ' + emObj.max_prob_3 + '</span><br/>' +
                    '<label>"notes" : </label> <span>' + ' ' + emObj.notes + '</span><br/>' +
                    '<label>"topwords_1" : </label> <span>' + ' ' + emObj.topwords_1 + '</span><br/>' +
                    '<label>"topwords_2" : </label> <span>' + ' ' + emObj.topwords_2 + '</span><br/>' +
                    '<label>"topwords_3" : </label> <span>' + ' ' + emObj.topwords_3 + '</span><br/>' +
                    '</div>';
                this.extractMeaningObj.formatData = '<div>' +
                    '<label>"topwords_1" : </label> <span>' + ' ' + emObj.topwords_1 + '</span><br/>' +
                    '<label>"topwords_2" : </label> <span>' + ' ' + emObj.topwords_2 + '</span><br/>' +
                    '<label>"topwords_3" : </label> <span>' + ' ' + emObj.topwords_3 + '</span><br/>' +
                    '</div>';

                if (extractMeaningMode == 2) {
                    this.uploadedFileContent = '';
                    //make all input fieled enable again
                    this.extractMeaningObj.textAreaDisabled = false;
                    //reset the file input
                    this.myExtractMeaningInputVariable.nativeElement.value = "";
                }
            })
        }
    }

    public computeRelatednessResult(computeRelatednessObj: ComputeRelatedness) {
        this.computeRelatednessObj.resultVisible = true;
        this.computeRelatednessObj.jsonData = '<div> {<br/>' +
            '&nbsp;&nbsp;<label>"Input" : </label> <span>' + ' ' + computeRelatednessObj.text + '</span><br/>' +
            '&nbsp;&nbsp;<label>"Scroes" : </label> [<br/>' +
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br/>' +
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>"Query" : </label> <span>' + ' ' + computeRelatednessObj.query + '</span><br/>' +
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>"Score" : </label> <span>' + ' ' + '0.997527375' + '</span><br/>' +
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br/>' +
            '&nbsp;&nbsp;&nbsp;]<br/>' +
            '}</div>';
        this.computeRelatednessObj.formatData = '<div>' +
            '<img style="width:35%;" src="../../assets/img/horizontal bar.png" /><br/>' +
            '<label>Input : </label> <span>' + ' ' + computeRelatednessObj.text + '</span><br/>' +
            '<label>Query : </label> <span>' + ' ' + computeRelatednessObj.query + '</span><br/>' +
            '<label>Score : </label> <span>' + ' ' + '99%' + '</span><br/>' +
            '</div>';
    }

    public SummarizeResult(summarizeObj: Summarize) {
        summarizeObj.ssNumberError = false;
        summarizeObj.resultVisible = false;

        let summarizeData: string = null;
        let summarizeMode: number = 0;

        if (summarizeObj.pastedText && summarizeObj.pastedText != '') {
            summarizeData = summarizeObj.pastedText;
            summarizeMode = 1;
        }
        else if (this.uploadedFileContent != '') {
            summarizeData = this.uploadedFileContent;
            summarizeMode = 2;
        }

        if (summarizeObj.summarizedSentence && summarizeObj.summarizedSentence != '') {
            if (summarizeObj.inputPageURL && summarizeObj.inputPageURL != '') {
                this.apiService.summarizeWebUrl(summarizeObj.inputPageURL, summarizeObj.summarizedSentence).then(res => {
                    if (res == "Error!") { console.log("Error occured at server."); return; }

                    let summarizeResultObj = <SummarizeResultModel>JSON.parse(res._body);
                    summarizeObj.resultVisible = true;
                    summarizeObj.result = summarizeResultObj.sm_api_content;
                })
            }

            if (summarizeMode != 0) {
                this.apiService.summarizeText(summarizeData, summarizeObj.summarizedSentence).then(res => {
                    if (res == "Error!") { console.log("Error occured at server."); return; }

                    if (summarizeMode == 2) {
                        this.uploadedFileContent = '';
                        //make all input fieled enable again
                        this.summarizeObj.textAreaDisabled = false;
                        this.summarizeObj.urlDisabled = false;
                        //reset the file input
                        this.mySummarizeInputVariable.nativeElement.value = "";
                    }
                    summarizeObj.resultVisible = true;
                    summarizeObj.result = res._body
                })
            }
        } else {
            summarizeObj.ssNumberError = true;
            summarizeObj.ssNumberErrorMessage = "Please enter type summarized sentence number";
        }
    }

    public sentimentAnalysisResult(sentimentAnalysisObj: SentimentAnalysis) {
        this.sentimentAnalysisObj.resultVisible = false;

        let sentimentAnalysisData: string = null;
        let analysisMode: number = 0;
        if (sentimentAnalysisObj.enteredText && sentimentAnalysisObj.enteredText != '') {
            sentimentAnalysisData = sentimentAnalysisObj.enteredText;
            analysisMode = 1;
        }
        else if (this.uploadedFileContent != '') {
            sentimentAnalysisData = this.uploadedFileContent;
            analysisMode = 2;
        }

        if (analysisMode != 0) {
            this.apiService.sentimentText(sentimentAnalysisData).then(res => {
                if (res == "Error!") { console.log("Error occured at server."); return; }

                sentimentAnalysisObj.resultVisible = true;
                let sentimentAnalysisResultObj = <SentimentAnalysisResultModel>JSON.parse(JSON.parse(res._body));

                this.sentimentAnalysisObj.jsonData = '<div>' +
                    '<label>"Interval" : </label> <span>' + ' ' + sentimentAnalysisData + '</span><br/>' +
                    '<label>"Probability" : </label> {<br/>' +
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>"Positive" : </label> <span>' + ' ' + sentimentAnalysisResultObj.Positive + '</span><br/>' +
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>"Negative" : </label> <span>' + ' ' + sentimentAnalysisResultObj.Negative + '</span><br/>' +
                    '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>"Neutral" : </label> <span>' + ' ' + sentimentAnalysisResultObj.Neutral + '</span><br/>' +
                    '&nbsp;&nbsp;}<br/>' +
                    '}</div>';
                this.sentimentAnalysisObj.formatData = '<div>' +
                    '<label>Interval : </label> <span>' + ' ' + sentimentAnalysisData + '</span><br/>' +
                    '<label>Positive : </label> <span>' + ' ' + sentimentAnalysisResultObj.Positive + '</span><br/>' +
                    '<label>Negative : </label> <span>' + ' ' + sentimentAnalysisResultObj.Negative + '</span><br/>' +
                    '<label>Neutral : </label> <span>' + ' ' + sentimentAnalysisResultObj.Neutral + '</span><br/>' +
                    '</div>';

                let Stats: any[] = [
                    { text: "negative", value: sentimentAnalysisResultObj.Negative },
                    { text: "positive", value: sentimentAnalysisResultObj.Positive },
                    { text: "neutral", value: sentimentAnalysisResultObj.Neutral }
                ];

                setTimeout(() => { this.initSvg(); this.drawPie(Stats); }, 1000);

                if (analysisMode == 2) {
                    this.uploadedFileContent = '';
                    //make all input fieled enable again
                    this.sentimentAnalysisObj.textAreaDisabled = false;
                    //reset the file input
                    this.mySentimentAnalysisInputVariable.nativeElement.value = "";
                }
            })
        }
    }

    public upload(selectedFile: any) {
        let file: File = selectedFile.files[0];
        let myReader: FileReader = new FileReader();
        myReader.onloadend = (e) => {
            let image_base64: string = myReader.result;
            let splitBase64: string[] = image_base64.split(",");
            this.uploadedFileContent = atob(splitBase64[1]);
            
            switch (this.selectedIndex) {
                case 0:
                    this.extractMeaningObj.textAreaDisabled = true;
                    break;
                case 1: break;
                case 2:
                    this.summarizeObj.textAreaDisabled = true;
                    this.summarizeObj.urlDisabled = true;
                    break;
                case 3:
                    this.sentimentAnalysisObj.textAreaDisabled = true;
                    break;
                default: break;
            }
        }
        myReader.readAsDataURL(file);
    }

    private initSvg() {
        this.width = 150; this.height = 150;
        this.radius = Math.min(this.width, this.height) / 2;
        this.color = d3Scale.scaleOrdinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
        this.arc = d3Shape.arc().outerRadius(this.radius - 10).innerRadius(0);
        this.pie = d3Shape.pie().sort(null).value((d: any) => d.value); this.svg = d3.select("svg").append("g").attr("transform", "translate(" + this.width / 2 + "," + this.height / 2 + ")");;
    }

    private drawPie(Stats: any) {
        let legendItemSize = 14;
        let legendSpacing = 4;
        let g = this.svg.selectAll(".arc").data(this.pie(Stats)).enter().append("g").attr("class", "arc");
        g.append("path").attr("d", this.arc).style("fill", (d: any) => this.color(d.data.text));

        let legend = this.svg
            .selectAll('.legend')
            .data(this.color.domain())
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', (d, i) => {
                let height = legendItemSize + legendSpacing
                let offset = height * this.color.domain().length / 2
                let x = legendItemSize * 8;
                let y = (i * height) - offset
                return `translate(${x}, ${y})`
            })

        legend
            .append('rect')
            .attr('width', legendItemSize)
            .attr('height', legendItemSize)
            .style('fill', this.color);

        legend
            .append('text')
            .attr('x', legendItemSize + legendSpacing)
            .attr('y', legendItemSize - legendSpacing)
            .text((d) => d)
    }
}

export class ExtractMeaning {
    enteredText: string;
    jsonData: any;
    formatData: any;
    resultVisible: boolean = false;
    textAreaDisabled: boolean = false;
    fileDisabled: boolean = false;
}

export class ComputeRelatedness {
    text: string;
    query: string;
    jsonData: any;
    formatData: any;
    resultVisible: boolean = false;
}

export class Summarize {
    inputPageURL: string;
    pastedText: string;
    summarizedSentence: string;
    result: string;
    resultVisible: boolean = false;
    ssNumberError: boolean = false;
    ssNumberErrorMessage: string;
    urlDisabled: boolean = false;
    textAreaDisabled: boolean = false;
    fileDisabled: boolean = false;
}

export class SentimentAnalysis {
    enteredText: string;
    jsonData: any;
    formatData: any;
    resultVisible: boolean = false;
    textAreaDisabled: boolean = false;
    fileDisabled: boolean = false;
}

export class ExtractMeaningResultModel {
    cluster_no_1: number;
    cluster_no_2: number;
    cluster_no_3: number;
    max_prob_1: number
    max_prob_2: number
    max_prob_3: number
    notes: string;
    topwords_1: string;
    topwords_2: string;
    topwords_3: string;
}

export class SummarizeResultModel {
    sm_api_character_count: string;
    sm_api_title: string;
    sm_api_content: string;
    sm_api_limitation: string;
}

export class SentimentAnalysisResultModel {
    Net_Sentiment: string;
    Positive: string;
    Negative: string;
    Neutral: string;
}
