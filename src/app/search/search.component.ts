import { Component, OnInit } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { ApiService } from '../Services/api.service';
import { Router } from '@angular/router';
import { DataService } from '../Services/dataService';

export class Data {
    id: string;
    text: string;
    type: string;
    source: number;
    fileName: string;
    frequencyCount: string;
    includeKeys: string[];
}

@Pipe({
    name: 'filterByText',
    pure: false
})

export class FilterByText implements PipeTransform {
    transform(items: any[], criteria: any): any {
        console.log(criteria);
        if (!items || !criteria) {
            return items;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return items.filter(item => {
            for (let key in item) {
                if (("" + item[key]).includes(criteria)) {
                    return true;
                }
            }
            return false;
        });
    }
}

@Pipe({
    name: 'filterByType',
    pure: false
})

export class FilterByType implements PipeTransform {
    transform(items: any[], criteria: any[]): any {
        if (!items || !criteria || criteria == null || criteria == undefined || criteria.length == 0) {
            return items;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return items.filter(item => {
            for (let key in item) {
                for (let c of criteria) {
                    if (c['itemName'] == ("" + item[key])) {
                        return true;
                    }
                }
            }
            return false;
        });
    }
}

@Pipe({
    name: 'filterByIncludedKeywords',
    pure: false
})

export class FilterByIncludedKeywords implements PipeTransform {
    transform(items: any[], criteria: any[]): any {
        if (!items || !criteria || criteria == null || criteria == undefined || criteria.length == 0) {
            return items;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return items.filter(item => {
            for (let key in item) {
                for (let c of criteria) {
                    if (("" + item[key]).includes(c)) {
                        return true;
                    }
                }
            }
            return false;
        });
    }
}

@Pipe({
    name: 'filterByExcludedKeywords',
    pure: false
})

export class FilterByExcludedKeywords implements PipeTransform {
    transform(items: any[], criteria: any): any {
        if (!items || !criteria || criteria == null || criteria == undefined || criteria.length == 0) {
            return items;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return items.filter(item => {
            for (let key in item) {
                for (let c of criteria) {
                    if (("" + item[key]).includes(c)) {
                        return false;
                    }
                }
            }
            return true;
        });
    }
}

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform {
    transform(items: any, filter: any, filterItems: Array<any>, isAnd: boolean): any {
        if (filter && Array.isArray(items) && filterItems) {
            let filterKeys = Object.keys(filter);
            let checkedItems = filterItems.filter(item => { return item.checked; });
            if (!checkedItems || checkedItems.length === 0) { return items; }
            if (isAnd) {
                return items.filter(item =>
                    filterKeys.reduce((acc1, keyName) =>
                        (acc1 && checkedItems.reduce((acc2, checkedItem) => acc2 && new RegExp(item[keyName], 'gi').test(checkedItem.value) || checkedItem.value === "", true))
                        , true)
                );
            } else {
                return items.filter(item => {
                    return filterKeys.some((keyName) => {
                        return checkedItems.some((checkedItem) => {
                            return new RegExp(item[keyName], 'gi').test(checkedItem.value) || checkedItem.value === "";
                        });
                    });
                });
            }
        } else {
            return items;
        }
    }
}

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})


export class SearchComponent implements OnInit {
    constructor(
        private apiService: ApiService,
        private router: Router,
        public dataservice: DataService) {
    }
    dropdownList = [];
    sourceList = [];
    includeTags = [];
    excludeTags = [];
    includeKeywords;
    excludeKeywords;
    resultCount;
    grid = true;

    pageSize = 9;
    pageNumber = 1;
    totalPageCount = 0;
    isNextBtnVisible = false;
    isPrevBtnVisible = false;

    results: Data[] = [];

    ngOnInit() {
        this.dropdownList = [
            { "id": 1, "itemName": "pdf" },
            { "id": 2, "itemName": "txt" },
            { "id": 3, "itemName": "ppt" }
        ];

        this.sourceList = [
            { "value": 1, "name": "User Content", "checked": false },
            { "value": 2, "name": "Internal Reports", "checked": false },
            { "value": 3, "name": "Emails", "checked": false },
            { "value": 4, "name": "Social Media", "checked": false },
            { "value": 5, "name": "External Reports", "checked": false }
        ];
        this.resultCount = 0;
    }

    selectedTypes;

    addIncludingTag(value) {
        if (value) {
            this.includeTags.push(value);
            this.includeKeywords = "";
        }
    }

    addExcludingTag(value) {
        if (value) {
            this.excludeTags.push(value);
            this.excludeKeywords = "";
        }
    }

    removeFromIncludeTags(value) {
        if (value) {
            this.includeTags.splice(value, 1);
        }
    }

    removeFromExcludeTags(value) {
        if (value) {
            this.excludeTags.splice(value, 1);
        }
    }

    checked() {
        return this.sourceList.filter(item => { return item.checked; });
    }

    public listView($event) {
        $event.preventDefault();
        this.grid = false;
    }

    public gridView($event) {
        $event.preventDefault();
        this.grid = true;
    }

    public download(fileName) {
        console.log(fileName);
        this.apiService.download(fileName);
    }

    public search($event, callingModule) {

        switch (callingModule) {
            case 1: this.isNextBtnVisible = false; this.isPrevBtnVisible = false; break;
            case 2: this.pageNumber--; break;
            case 3: this.pageNumber++; break;
            default: break;
        }

        let documentTypes = [];
        let sources = [];
        for (let entry of this.selectedTypes) {
            documentTypes.push(entry.itemName)
        }

        for (let entry of this.sourceList) {
            if (entry.checked) {
                sources.push(entry.name)
            }
        }

        console.log(documentTypes);
        console.log(sources);
        console.log(this.selectedTypes);
        console.log(this.sourceList);
        console.log(this.includeTags);
        console.log(this.excludeTags);

        this.apiService.search(documentTypes, sources, this.includeTags, this.excludeTags, (this.pageNumber - 1)).then(res => {
            if (res == "Error!") {
                console.log("Error occured at server.");
                return;
            }
            //Reset all fields
            this.results = [];
            
            switch (callingModule) {
                case 1:
                    this.resultCount = res.response.numFound;
                    let totalPagesFloatValue = res.response.numFound / this.pageSize;
                    this.totalPageCount = Math.floor(totalPagesFloatValue % 1 == 0 ? totalPagesFloatValue : totalPagesFloatValue + 1);
                    if (this.totalPageCount > 1) { this.isNextBtnVisible = true; }
                    break;
                case 2:
                    this.isNextBtnVisible = true;
                    if (this.pageNumber == 1) { this.isPrevBtnVisible = false; }
                    break;
                case 3:
                    this.isPrevBtnVisible = true;
                    if (this.totalPageCount == this.pageNumber) { this.isNextBtnVisible = false; }
                    break;
                default: break;
            }

            if (this.resultCount > 0) {
                for (let entry of res.response.docs) {
                    this.results.push({ id: entry.id, text: res.highlighting[entry.id].fileData[0], type: 'txt', source: 1, fileName: entry.fileName, frequencyCount: entry.frequencyCount, includeKeys: res.includeKeys })
                }
            }
        })
    }

    public analyze(selectedObj) {
        this.dataservice.searchData = selectedObj;
        this.router.navigate(['/analyze']);
    }
}
