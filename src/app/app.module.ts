﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router'
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MasterComponent } from './Shared/master/master.component';
import { AuthGuard } from './Services/auth.guard';
import { SearchComponent } from './search/search.component';
import { FilterByText } from './search/search.component';
import { FilterByType } from './search/search.component';
import { FilterByExcludedKeywords } from './search/search.component';
import { FilterPipe } from './search/search.component';
import { FilterByIncludedKeywords } from './search/search.component';
import { DiscoverComponent } from './discover/discover.component';
import { ApiService } from './Services/api.service';
import { HttpService } from './Services/http.service';
import { DataService } from './Services/dataService';
import { AnalyzeComponent } from './analyze/analyze.component';

const appRoute: Routes = [

  { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'search', component: SearchComponent, canActivate: [AuthGuard] },
  { path: 'discover', component: DiscoverComponent, canActivate: [AuthGuard] },
  { path: 'analyze', component: AnalyzeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent }

];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MasterComponent,
    DashboardComponent,
    SearchComponent,
    FilterByText,
    FilterByType,
    FilterByExcludedKeywords,
    FilterPipe,
    FilterByIncludedKeywords,
    DiscoverComponent,
    AnalyzeComponent
  ],
  imports: [
    BrowserModule, HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoute),
    AngularMultiSelectModule
  ],
  providers: [AuthGuard, ApiService, HttpService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
