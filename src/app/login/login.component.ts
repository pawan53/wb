﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../Services/api.service';
import { MasterComponent } from '../Shared/master/master.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent  {

    username: string;
    password: string;
    errorMessage: string;

    constructor(private router: Router, private master: MasterComponent, private apiService: ApiService) { }

    public login($event) {
        this.apiService.login(this.username, this.password)
            .then(res => {
                if (res.statusCode == 200) {
                    this.master.setIsAuth(true);
                    localStorage.setItem("isAuth", "true");
                    this.router.navigate(['/dashboard']);
                } else {
                    this.master.setIsAuth(false);
                    this.password = '';
                    this.errorMessage = 'Invalid username or password.';
                }
            })
    }
}
